package ru.jankbyte.sample.addresscache.config

import com.fasterxml.jackson.databind.ObjectMapper
import io.ktor.client.*
import io.ktor.client.engine.apache.*
import io.ktor.server.application.*
import io.ktor.util.*
import org.slf4j.LoggerFactory
import redis.clients.jedis.HostAndPort
import redis.clients.jedis.Jedis
import redis.clients.jedis.params.SetParams
import ru.jankbyte.sample.addresscache.service.HttpRequestsService
import ru.jankbyte.sample.addresscache.service.JedisService
import ru.jankbyte.sample.addresscache.service.RequestProcessorService

val REDIS_INSTANCE = AttributeKey<Jedis>("jedis")
val Application.jedis: Jedis get() = attributes[REDIS_INSTANCE]

fun Application.configureJedis() {
    val logger = LoggerFactory.getLogger(this::class.java)
    val portStr = environment.config.propertyOrNull("jedis.port")?.getString()
    val port = Integer.parseInt(portStr)
    val host = environment.config.propertyOrNull("jedis.host")?.getString()
    val hostAndPort = HostAndPort(host, port)
    logger.info("Connection of jedis: ${host}:${port}")
    val redis = Jedis(hostAndPort)
    attributes.put(REDIS_INSTANCE, redis)
}

val HTTP_CLIENT = AttributeKey<HttpClient>("httpClient")
val Application.httpClient: HttpClient get() = attributes[HTTP_CLIENT]

fun Application.configureHttpClient() {
    val httpClient = HttpClient(Apache) {
        engine {
            followRedirects = true
            socketTimeout = 10_000
            connectTimeout = 10_000
            connectionRequestTimeout = 20_000
            customizeClient {
                setMaxConnTotal(1000)
                setMaxConnPerRoute(100)
            }
            customizeRequest {}
        }
    }
    attributes.put(HTTP_CLIENT, httpClient)
}

val HTTP_REQUEST_SERVICE = AttributeKey<HttpRequestsService>("httpRequestService")
val Application.httpRequestService: HttpRequestsService get() = attributes[HTTP_REQUEST_SERVICE]

fun Application.configureHttpRequestsService() {
    val apiKey = environment.config.propertyOrNull("address-service.api-key")?.getString() ?: error("api key not found")
    val secret = environment.config.propertyOrNull("address-service.secret")?.getString() ?: error("secret key not found")
    val baseUrl = environment.config.propertyOrNull("address-service.base-url")?.getString() ?: error("base Url key not found")
    val client = HttpRequestsService(httpClient, apiKey, secret, baseUrl)
    attributes.put(HTTP_REQUEST_SERVICE, client)
}

val OBJECT_MAPPER = AttributeKey<ObjectMapper>("objectMapper")
val Application.objectMapper: ObjectMapper get() = attributes[OBJECT_MAPPER]

fun Application.configureObjectMapper() {
    val jsonMapper = ObjectMapper()
    attributes.put(OBJECT_MAPPER, jsonMapper)
}

val REQUEST_PROCESSOR_SERVICE = AttributeKey<RequestProcessorService>("requestProcessorService")
val Application.requestProcessorService: RequestProcessorService get() = attributes[REQUEST_PROCESSOR_SERVICE]

fun Application.configureRequestProcessorService() {
    val requestProcService = RequestProcessorService(httpRequestService, jedisService, objectMapper)
    attributes.put(REQUEST_PROCESSOR_SERVICE, requestProcService)
}

val JEDIS_SERVICE = AttributeKey<JedisService>("jedisService")
val Application.jedisService: JedisService get() = attributes[JEDIS_SERVICE]

fun Application.configureJedisService() {
    val expiresKey = environment.config.propertyOrNull("jedis.expires-after")?.getString() ?: error("expires key not found")
    val logger = LoggerFactory.getLogger(this::class.java)
    val expiresSeconds = expiresKey.toLong()
    logger.info("Configured expiring: $expiresSeconds")
    val params = SetParams()
    params.ex(expiresSeconds)
    val jedisService = JedisService(jedis, params)
    attributes.put(JEDIS_SERVICE, jedisService)
}