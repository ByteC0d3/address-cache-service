package ru.jankbyte.sample.addresscache.service

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking

/**
 * The service with
 */
class HttpRequestsService(private val client: HttpClient,
                          private val apiKey: String,
                          private val secret: String,
                          private val baseUrl: String
) {
    fun getAddresses(address: String): String {
        return doPostInOtherThread(address)
    }

    private fun doPostInOtherThread(body: String): String = runBlocking {
        client.post("$baseUrl/suggestions/api/4_1/rs/suggest/address") {
            contentType(ContentType.Application.Json)
            setBody(body)
            headers {
                append("Content-Type", "application/json")
                append("Authorization", "Token $apiKey")
                append("X-Secret", "secret")
            }
        }.body<String>().toString()
    }
}