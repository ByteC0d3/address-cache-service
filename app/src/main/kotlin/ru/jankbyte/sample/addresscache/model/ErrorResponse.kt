package ru.jankbyte.sample.addresscache.model

data class ErrorResponse(val error: String)
