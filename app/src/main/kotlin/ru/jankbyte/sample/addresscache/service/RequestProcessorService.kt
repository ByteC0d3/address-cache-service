package ru.jankbyte.sample.addresscache.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import redis.clients.jedis.Jedis

class RequestProcessorService(private val httpService: HttpRequestsService,
                              private val redisService: JedisService,
                              private val mapper: ObjectMapper
) {
    private val logger = LoggerFactory.getLogger(this::class.java)
    fun processGettingAddressRequest(searchAddress: String): String {
        val lower = searchAddress.lowercase()
        val addressFromCache = redisService.get(lower)
        return if (addressFromCache != null) {
            logger.info("Loading data from redis")
            addressFromCache
        } else {
            val json = """
                { "query" : "$lower" }
                """.trimIndent()
            logger.info("Loading data from server: $json")
            val addressFromResponse = httpService.getAddresses(json)
            redisService.set(lower, addressFromResponse)
            addressFromResponse
        }

    }
}