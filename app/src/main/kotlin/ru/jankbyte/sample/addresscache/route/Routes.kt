package ru.jankbyte.sample.addresscache.route

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.slf4j.LoggerFactory
import ru.jankbyte.sample.addresscache.config.objectMapper
import ru.jankbyte.sample.addresscache.config.requestProcessorService
import ru.jankbyte.sample.addresscache.model.ErrorResponse
import kotlin.Exception


/**
 * Configure HTTP-routes.
 */
fun Application.configureRoutes() {
    val logger = LoggerFactory.getLogger(this::class.java)
    val reqProcService = requestProcessorService
    val mapper = objectMapper
    routing {
        get("/getAddress") {
            logger.info("Received getAddress route")
            try {
                // TODO: remove duplicates (like writeValueAsString)
                val address: String = call.request
                        .queryParameters["address"] ?: error("Address is empty")
                logger.info("The address for searching: ${address.toString()}")
                val addressResponse = reqProcService.processGettingAddressRequest(address)
                call.respondText(addressResponse)
            } catch (e: Exception) {
                logger.warn("Something wrong: (${e.javaClass.name}) ${e.message.toString()}")
                val errorResponse = ErrorResponse(e.message.toString())
                val errorResponseText = mapper.writeValueAsString(errorResponse)
                call.respondText(errorResponseText)
            }
        }
    }
}