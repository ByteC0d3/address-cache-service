package ru.jankbyte.sample.addresscache.service

import redis.clients.jedis.Jedis
import redis.clients.jedis.params.SetParams

class JedisService(private val jedis: Jedis,
                   private val params: SetParams) {
    fun set(key: String, value: String) {
        jedis.set(key, value, params)
    }

    fun get(key: String): String? {
        return jedis.get(key)
    }
}