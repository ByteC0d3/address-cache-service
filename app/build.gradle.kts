import org.jetbrains.kotlin.ir.backend.js.transformers.irToJs.argumentsWithVarargAsSingleArray
import kotlin.script.experimental.jvm.defaultJvmScriptingHostConfiguration

plugins {
    id("io.ktor.plugin") version "2.3.1"
    id("org.jetbrains.kotlin.jvm") version "1.8.10"
}

repositories {
    mavenCentral()
}

dependencies {
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit5")
    testImplementation("org.junit.jupiter:junit-jupiter-engine:5.9.1")
    implementation("redis.clients:jedis:4.4.3")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.15.2")

    // ktor
    implementation("io.ktor:ktor-server-core-jvm")
    implementation("io.ktor:ktor-server-netty-jvm")
    implementation("io.ktor:ktor-client-core-jvm")
    implementation("io.ktor:ktor-client-apache")

    // logger
    implementation("org.slf4j:slf4j-simple:2.0.7")
    implementation("ch.qos.logback:logback-classic:1.4.8")

}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

application {
    mainClass.set("ru.jankbyte.sample.addresscache.AppKt")
}

tasks.named<Test>("test") {
    useJUnitPlatform()
}
