FROM openjdk:17
ADD app/build/libs/app-all.jar .
WORKDIR .
ENV PORT=8080
ENV EXPIRING_TIME=70
ENV API_KEY=404fae6aaf332a73f8720a7500f91ef5bdd820af
ENV SECRET=e14a563e292852de670020e56e389018747fa1cd
ENV REDIS_HOST=localhost
ENV REDIS_PORT=6379
ENV BASE_URL=https://suggestions.dadata.ru
RUN export EXPIRING_TIME=${EXPIRING_TIME}
RUN export API_KEY=${API_KEY}
RUN export SECRET=${SECRET}
RUN export REDIS_HOST=${REDIS_HOST}
RUN export REDIS_PORT=${REDIS_PORT}
RUN export BASE_URL=${BASE_URL}
RUN export PORT=${PORT}
CMD ["java", "-jar", "app-all.jar"]